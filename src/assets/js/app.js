$(document).ready(function(){
	var $scroll = $('.js-scroll-to');

	$('.js-feature-content__headline-1').click(function() {
		$('.js-feature-more-1').fadeToggle();
		$('.js-feature-more-2').fadeOut();
		$('.js-feature-more-3').fadeOut();
		$('.js-feature-more-4').fadeOut();
	});

	$('.js-feature-content__headline-2').click(function() {
		$('.js-feature-more-2').fadeToggle();
		$('.js-feature-more-1').fadeOut();
		$('.js-feature-more-3').fadeOut();
		$('.js-feature-more-4').fadeOut();
	});

	$('.js-feature-content__headline-3').click(function() {
		$('.js-feature-more-3').fadeToggle();
		$('.js-feature-more-2').fadeOut();
		$('.js-feature-more-1').fadeOut();
		$('.js-feature-more-4').fadeOut();
	});

	$('.js-feature-content__headline-4').click(function() {
		$('.js-feature-more-4').fadeToggle();
		$('.js-feature-more-2').fadeOut();
		$('.js-feature-more-3').fadeOut();
		$('.js-feature-more-1').fadeOut();
	});

	$scroll.click(function(e){
		e.preventDefault();

		$('html, body').animate({
			scrollTop: $(this.hash).offset().top
		}, 1000);
	});
})
